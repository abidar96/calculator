# Fonctionnalités

- Développer des nouvelles opérations comme modulo, racine, puissance ... (Le code est écrit d'une manière qui facilite l'ajout de nouvelles opérations)
- Ajouter plusieurs valeurs à la fois dans la stack
- Appliquer plusieurs opération successivement
- Création d'une nouvelle stack en copiant une autre (stack copie)
- Supprimer toutes les stacks
- Ajouter une base de données (SQL ou NoSql) pour avoir de la persistence de données
- Passer en .Net Core 7
- Ajouter du logging
- Ajouter des tests d'integration
