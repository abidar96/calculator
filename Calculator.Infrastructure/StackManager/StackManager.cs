﻿using Calculator.Domain.StacksManager;

namespace Calculator.Infrastructure.StackManager;

public class StackManager : IStackManager
{
    private int _genId = 0;
    private readonly Dictionary<int, Stack<int>> _stacks = new ();

    public int AddStack()
    {
        var stack = new Stack<int>();
        _stacks.Add(++_genId, stack);
        return _genId;
    }

    public bool ContainsStack(int stackId)
    {
        return _stacks.ContainsKey(stackId);
    }

    public Stack<int> Get(int id)
    {
        if (!_stacks.ContainsKey(id))
        {
            throw new KeyNotFoundException($"Could not find the requested stack id {id}");
        }
        return _stacks[id];

    }

    public IDictionary<int, Stack<int>> GetAllStacks()
    {
        return new Dictionary<int, Stack<int>>(_stacks);
    }

    public bool Remove(int stackId)
    {
        return _stacks.Remove(stackId);
    }
}
