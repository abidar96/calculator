﻿namespace Calculator.Domain.Operand;

public interface IOperand
{
    string GetOperand();
    bool CanExecute(string operand)
    {
        return operand == GetOperand();
    }
    int Execute(int valA, int valB);
}
