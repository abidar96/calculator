﻿namespace Calculator.Domain.StacksManager;

public interface IStackManager
{
    int AddStack();

    Stack<int> Get(int id);

    IDictionary<int, Stack<int>> GetAllStacks();

    bool ContainsStack(int stackId);

    bool Remove(int stackId);
}
