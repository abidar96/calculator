# Calculator

Objectif :

Réalisation d’une calculatrice RPN (notation polonaise inversée) en mode client/serveur

Langages :

Backend : API REST, c#, .Net Core

Frontend : Swagger

Fonctionnalités demandées :

Ajout d’un élément dans une pile

Récupération de la pile

Nettoyage de la pile

Opération +

Opération -

Opération *

Opération /

Livrables :

Code : Livraison du code sur un repo github à nous communiquer

ToDo : Fichier « todo.md » listant les améliorations et les raccourcis pris à cause du temps imparti.

Roadmap : Fichier « roadmap.md » listant quelques fonctionnalités qui pourraient être apportées au projet pour constituer une première backlog.

Principe de fonctionnement d’une calculatrice RPN :

Stack : []

Push de 10

Stack : [ 10 ]

Push de 5

Stack : [ 10, 5 ]

Push de 6

Stack : [ 10, 5, 6 ]

Operand ‘+’

Stack : [ 10, 11 ]