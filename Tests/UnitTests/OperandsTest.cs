using Calculator.Domain.Operand;
using NUnit.Framework;

namespace UnitTests;

public class OperandsTest
{
    [Test]
    public void AdditionOperandTest()
    {
        //Arrange
        IOperand operand = new AdditionOperand();
        int valA = 3;
        int valB = 4;
        int expectedResult = 7;

        //ACT 
        var res = operand.Execute(valA, valB);

        //Assert
        Assert.IsTrue(operand.CanExecute("+"));
        Assert.IsFalse(operand.CanExecute("-"));
        Assert.IsFalse(operand.CanExecute("*"));
        Assert.IsFalse(operand.CanExecute("/"));
        Assert.AreEqual(expectedResult, res);
    }

    [Test]
    public void SubstractionOperandTest()
    {
        //Arrange
        IOperand operand = new SubstractionOperand();
        int valA = 7;
        int valB = 3;
        int expectedResult = 4;

        //ACT 
        var res = operand.Execute(valA, valB);

        //Assert
        Assert.IsTrue(operand.CanExecute("-"));
        Assert.IsFalse(operand.CanExecute("+"));
        Assert.IsFalse(operand.CanExecute("*"));
        Assert.IsFalse(operand.CanExecute("/"));
        Assert.AreEqual(expectedResult, res);
    }

    [Test]
    public void MultiplicationOperandTest()
    {
        //Arrange
        IOperand operand = new MultiplicationOperand();
        int valA = 3;
        int valB = 4;
        int expectedResult = 12;

        //ACT 
        var res = operand.Execute(valA, valB);

        //Assert
        Assert.IsTrue(operand.CanExecute("*"));
        Assert.IsFalse(operand.CanExecute("-"));
        Assert.IsFalse(operand.CanExecute("+"));
        Assert.IsFalse(operand.CanExecute("/"));
        Assert.AreEqual(expectedResult, res);
    }

    [Test]
    public void DivisionOperandTest()
    {
        //Arrange
        IOperand operand = new DivisionOperand();
        int valA = 10;
        int valB = 5;
        int expectedResult = 2;

        //ACT 
        var res = operand.Execute(valA, valB);

        //Assert
        Assert.IsTrue(operand.CanExecute("/"));
        Assert.IsFalse(operand.CanExecute("-"));
        Assert.IsFalse(operand.CanExecute("*"));
        Assert.IsFalse(operand.CanExecute("+"));
        Assert.AreEqual(expectedResult, res);
    }
}
