using Calculator.Api.Service;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Calculator.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RpnController : ControllerBase
    {
        private readonly IOperandService _operandService;


        public RpnController(IOperandService operandService)
        {
            _operandService = operandService;
        }

        [SwaggerOperation(Summary = "Get All operands")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<string>), 200)]
        [HttpGet("/op")]
        public IEnumerable<string> Get()
        {
            return _operandService.GetAllOperands();
        }

        [SwaggerOperation(Summary = "Create a new stack")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(int), 200)]
        [HttpPost("/stack")]
        public int CreateNewStack()
        {
            return _operandService.AddStack();
        }

        [SwaggerOperation(Summary = "Get All stacks")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IDictionary<int, Stack<int>>), 200)]
        [HttpGet("/stack")]
        public IDictionary<int, Stack<int>> GetAllStacks()
        {
            return _operandService.GetAllStacks();
        }

        [SwaggerOperation(Summary = "Get a stack by it's id")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Stack<int>), 200)]
        [HttpGet("/stack/{stackId}")]
        public Stack<int> GetStackById([FromRoute] int stackId)
        {
            return _operandService.GetStackById(stackId);
        }

        [SwaggerOperation(Summary = "Push an element to stack")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Stack<int>), 200)]
        [HttpPost("/stack/{stackId}")]
        public Stack<int> PushToStack([FromRoute] int stackId, [FromQuery] int value)
        {
            return _operandService.PushToStack(stackId, value);
        }

        [SwaggerOperation(Summary = "Remove a stack by it's id")]
        [Produces("application/json")]
        [ProducesResponseType(204)]
        [HttpDelete("/stack/{stackId}")]
        public ActionResult RemoveStack([FromRoute] int stackId)
        {
            _operandService.RemoveStackById(stackId);
            return NoContent();
        }

        [SwaggerOperation(Summary = "Apply an operand on a given stack")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Stack<int>), 200)]
        [HttpPost("/op/{operand}/stack/{stackId}")]
        public Stack<int> ApplyOperand([FromRoute] string operand, [FromRoute] int stackId)
        {
            return _operandService.ApplyOperandOnStack(stackId, operand);
        }

    }
}