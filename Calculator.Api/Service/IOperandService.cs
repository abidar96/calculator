﻿namespace Calculator.Api.Service;

public interface IOperandService
{
    IEnumerable<string> GetAllOperands();

    int AddStack();

    IDictionary<int, Stack<int>> GetAllStacks();

    Stack<int> GetStackById(int stackId);

    Stack<int> PushToStack(int stackId, int value);

    void RemoveStackById(int stackId);

    Stack<int> ApplyOperandOnStack(int stackId, string operand);
}
