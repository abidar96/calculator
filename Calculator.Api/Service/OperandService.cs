﻿using Calculator.Api.Exceptions;
using Calculator.Domain.Exceptions;
using Calculator.Domain.Operand;
using Calculator.Domain.StacksManager;

namespace Calculator.Api.Service;

public class OperandService : IOperandService
{
    private readonly IEnumerable<IOperand> _operands;
    private readonly IStackManager _stackManager;

    public OperandService(IEnumerable<IOperand> operands, IStackManager stackManager)
    {
        _operands = operands;
        _stackManager = stackManager;
    }

    public int AddStack()
    {
        return _stackManager.AddStack();
    }

    public Stack<int> ApplyOperandOnStack(int stackId, string operand)
    {
        if (!_stackManager.ContainsStack(stackId))
            throw new NotFoundException("Stack not found");

        var stack = _stackManager.Get(stackId);

        if (stack.Count < 2)
            throw new FunctionalException("Stack must contains 2 elements at least!!");

        var operandHandler = _operands.SingleOrDefault(x => x.CanExecute(operand));

        if (operandHandler == null)
            throw new FunctionalException("No matching operand found");
        
        var valA = stack.Pop();
        var valB = stack.Pop();

        stack.Push(operandHandler.Execute(valA, valB));

        return stack;
    }

    public IEnumerable<string> GetAllOperands()
    {
        var listOperand = new List<string>();
        foreach (var operand in _operands)
            listOperand.Add(operand.GetOperand());
        return listOperand;
    }

    public IDictionary<int, Stack<int>> GetAllStacks()
    {
        return _stackManager.GetAllStacks();
    }

    public Stack<int> GetStackById(int stackId)
    {
        if (!_stackManager.ContainsStack(stackId))
            throw new NotFoundException("Stack not found");

        return _stackManager.Get(stackId);
    }

    public Stack<int> PushToStack(int stackId, int value)
    {
        if (!_stackManager.ContainsStack(stackId))
            throw new NotFoundException("Stack not found");

        var stack = _stackManager.Get(stackId);
        stack.Push(value);
        return stack;
    }

    public void RemoveStackById(int stackId)
    {
        if (!_stackManager.ContainsStack(stackId))
            throw new NotFoundException("Stack not found");

        _stackManager.Remove(stackId);
    }
}
