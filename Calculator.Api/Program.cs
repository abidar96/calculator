using Calculator.Api.Infrastructure.Errors;
using Calculator.Api.Service;
using Calculator.Domain.Operand;
using Calculator.Domain.StacksManager;
using Calculator.Infrastructure.StackManager;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddSwaggerGen(options =>
{
    options.DescribeAllParametersInCamelCase();
    options.SwaggerDoc("v1", new OpenApiInfo { Title = "Calculator Api", Version = "v1" });
    options.EnableAnnotations();
});

builder.Services.AddSingleton<IStackManager, StackManager>();
builder.Services.AddScoped<IOperandService, OperandService>();
builder.Services.AddScoped<IOperand, AdditionOperand>();
builder.Services.AddScoped<IOperand, SubstractionOperand>();
builder.Services.AddScoped<IOperand, MultiplicationOperand>();
builder.Services.AddScoped<IOperand, DivisionOperand>();

var app = builder.Build();

// Add Error Handler
app.UseExceptionHandler(application => application.UseErrors(app.Environment));

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseHttpsRedirection();
}

app.UseSwagger();
app.UseSwaggerUI();

app.MapControllers();

app.Run();
