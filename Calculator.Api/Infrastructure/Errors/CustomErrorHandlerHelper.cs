﻿using Calculator.Api.Exceptions;
using Calculator.Domain.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Text.Json;

namespace Calculator.Api.Infrastructure.Errors;

public static class CustomErrorHandlerHelper
{
    internal static Task WriteDevelopmentResponse(HttpContext httpContext, Func<Task> next)
    {
        _ = next;

        return WriteResponse(httpContext, includeDetails: true);
    }

    internal static Task WriteProductionResponse(HttpContext httpContext, Func<Task> next)
    {
        _ = next;

        return WriteResponse(httpContext, includeDetails: false);
    }

    private static async Task WriteResponse(HttpContext httpContext, bool includeDetails)
    {
        var exceptionDetails = httpContext.Features.Get<IExceptionHandlerFeature>();
        var exception = exceptionDetails?.Error;

        if (exception != null)
        {
            httpContext.Response.ContentType = "application/problem+json";

            var title = "The server encountered an internal error. Please try the request again.";
            var details = includeDetails ? exception.ToString() : null;
            int status;

            var problem = new ProblemDetails
            {
                Detail = details
            };

            if (exception is FunctionalException functionalException)
            {
                status = StatusCodes.Status400BadRequest;
                title = functionalException.Message;
            }
            else if (exception is NotFoundException notFoundException)
            {
                status = StatusCodes.Status404NotFound;
                title = notFoundException.Message;
            }
            else
            {
                status = StatusCodes.Status500InternalServerError;
            }

            problem.Title = title;

            var traceId = Activity.Current?.Id ?? httpContext?.TraceIdentifier;
            if (traceId != null)
                problem.Extensions["traceId"] = traceId;

            httpContext.Response.StatusCode = status;

            var stream = httpContext.Response.Body;
            await JsonSerializer.SerializeAsync(stream, problem).ConfigureAwait(false);
        }
    }
}
